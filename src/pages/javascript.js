import logo from '../logo.svg';
import '../App.css';
import Language from '../components/languageComponents';
import About from './about';
import {
    useParams,
} from "react-router-dom";

function Button() {
    let { id } = useParams();

    return (
        <div className="App">
            <header className="App-header">
                <h1>
                    Lesen die Programmiersprache
                </h1>
                <h1>ID: {id}</h1>
                <About
                    id={id}
                    name={'javascript'}
                />
            </header>
        </div>
    );
}

export default Button;